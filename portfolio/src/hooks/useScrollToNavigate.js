import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

const useScrollToNavigate = (routes, delay = 500) => {
  const navigate = useNavigate();
  const [timeoutId, setTimeoutId] = useState(null);
  const [currentIndex, setCurrentIndex] = useState(0);

  useEffect(() => {
    let prevScrollPos = window.pageYOffset;

    const handleScroll = () => {
      const currentScrollPos = window.pageYOffset;
      const scrollHeight = document.documentElement.scrollHeight;
      const clientHeight = document.documentElement.clientHeight;

      if (currentScrollPos + clientHeight >= scrollHeight) {
        // User has reached the bottom of the page
        const id = setTimeout(() => {
          setCurrentIndex((prevIndex) => {
            const nextIndex = prevIndex + 1;
            if (nextIndex < routes.length) {
              navigate(routes[nextIndex]);
            }
            return nextIndex;
          });
        }, delay);
        setTimeoutId(id);
      } else if (currentScrollPos < prevScrollPos) {
        // User is scrolling up
        clearTimeout(timeoutId);
        setCurrentIndex((prevIndex) => {
          const previIndex = prevIndex - 1;
          if (previIndex >= 0) {
            navigate(routes[previIndex]);
          }
          return prevIndex;
        });
      }

      prevScrollPos = currentScrollPos;
    };

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
      if (timeoutId) {
        clearTimeout(timeoutId);
      }
    };
  }, [navigate, routes, delay]);

  return currentIndex;
};

export default useScrollToNavigate;
