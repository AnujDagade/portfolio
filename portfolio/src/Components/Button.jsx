import React from 'react'

export default function Button({children,look}) {
  return (
    <span className={`rounded-full bg-violet-600 w-fit p-5 text-white ${look}`}><button >{children}</button></span>
  )
}
