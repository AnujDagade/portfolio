import React, { Children } from 'react'

export default function NavItems({children}) {
  return (
    <div>{children}</div>
  )
}
