import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Projects from './Projects';
import 'font-awesome/css/font-awesome.min.css';
import useScrollToNavigate from '../hooks/useScrollToNavigate';

const Home = () => {
  const [showMore, setShowMore] = useState(false);
  const navigate = useNavigate();
  // useScrollToNavigate(["/home","/projects","/contact","/about"]);

  return (
    <>
      <div className='pt-4 mt-10 flex justify-center'>
        <p className='text-2xl text-red-600'>Under development</p>
      </div>

      {/* Header */}
      <header className="relative z-10 text-center mt-20" id="navbar">
        <h1 className="text-6xl font-extrabold text-blue-400 mb-4 glow-md tracking-wide">Anuj Dagade</h1>
        <p className="text-lg text-gray-300 mt-4 leading-relaxed max-w-3xl mx-auto">
          <span className="font-bold text-blue-400">A passionate Developer</span> with expertise in building
          <span className="text-blue-400 font-semibold"> scalable applications</span> and
          <span className="text-blue-400 font-semibold"> modern user interfaces</span>.
          With a deep interest in <span className="font-semibold text-pink-400">problem-solving</span> and
          <span className="font-semibold text-pink-400"> backend architecture</span>,
          I love working on innovative projects that make an impact. When I'm not coding, I'm exploring new tools
          {/*contributing to open-source,*/} or experimenting with <span className="font-semibold text-red-400">automation in gaming</span>!
        </p>

        <div className="flex justify-center mt-6">
          <a
            href="https://www.linkedin.com/in/anuj-dagade" // Replace with your LinkedIn URL
            target="_blank"
            rel="noopener noreferrer"
            className="flex items-center bg-blue-500 hover:bg-blue-600 text-white font-bold py-2 px-4 rounded-full mr-4 transition duration-300"
          >
            <i className="fa fa-linkedin mr-2"></i> LinkedIn
          </a>
          <a
            href="https://github.com/AnujDagade" // Replace with your GitHub URL
            target="_blank"
            rel="noopener noreferrer"
            className="flex items-center bg-gray-800 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded-full transition duration-300"
          >
            <i className="fa fa-github mr-2"></i> GitHub
          </a>
        </div>

      </header>

      {/* Stylish Skills Section */}
      <section className="relative z-10 mt-10 text-center">
        <h2 className="text-4xl font-bold text-blue-400 mb-4">Skills</h2>
        <div className="grid grid-cols-1 md:grid-cols-2 gap-4 text-black">
          <div>
            <h3 className="text-xl font-semibold text-blue-500 mb-2">Frontend</h3>
            <p className="bg-blue-600 rounded-full py-1 px-3 inline-block">React</p>
            <p className="bg-blue-600 rounded-full py-1 px-3 inline-block ml-2">Angular</p>
            <p className="bg-blue-600 rounded-full py-1 px-3 inline-block ml-2">JavaScript</p>
            <p className="bg-blue-600 rounded-full py-1 px-3 inline-block ml-2">HTML</p>
            <p className="bg-blue-600 rounded-full py-1 px-3 inline-block ml-2">CSS</p>
          </div>
          <div>
            <h3 className="text-xl font-semibold text-green-500 mb-2">Backend</h3>
            <p className="bg-green-600 rounded-full py-1 px-3 inline-block">Node.js</p>
            <p className="bg-green-600 rounded-full py-1 px-3 inline-block ml-2">Java</p>
            <p className="bg-green-600 rounded-full py-1 px-3 inline-block ml-2">Spring</p>
            <p className="bg-green-600 rounded-full py-1 px-3 inline-block ml-2">Express.js</p>
          </div>
          <div>
            <h3 className="text-xl font-semibold text-purple-500 mb-2">Databases</h3>
            <p className="bg-purple-600 rounded-full py-1 px-3 inline-block">MongoDB</p>
            <p className="bg-purple-600 rounded-full py-1 px-3 inline-block ml-2">Postgresql</p>
            {/* <p className="bg-purple-600 rounded-full py-1 px-3 inline-block ml-2">Neo4j</p> */}
          </div>
          <div>
            <h3 className="text-xl font-semibold text-yellow-500 mb-2">Others</h3>
            <p className="bg-yellow-600 rounded-full py-1 px-3 inline-block">Python</p>
            <p className=" ml-2 bg-yellow-600 rounded-full py-1 px-3 inline-block">Bash</p>
            <p className="ml-2 bg-yellow-600 rounded-full py-1 px-3 inline-block">Git</p>
            <p className="bg-yellow-600 rounded-full py-1 px-3 inline-block ml-2">Docker</p>
            <p className="bg-yellow-600 rounded-full py-1 px-3 inline-block ml-2">Linux</p>
            <p className="bg-yellow-600 rounded-full py-1 px-3 inline-block ml-2">REST APIs</p>
          </div>
        </div>
      </section>

      {/* Projects Section */}
      <section className="relative z-10 mt-10">
        <Projects showMore={showMore} numProjects={2} />
        <button
          onClick={() => navigate("/projects")}
          className="mt-4 bg-blue-500 hover:bg-blue-600 text-white font-semibold py-2 px-4 rounded transition duration-300 ml-5"
        >
          {showMore ? "Show Less" : "Show More"}
        </button>
      </section>

      {/* Footer */}
      <footer className="relative z-10 mt-20 text-center text-gray-500">
        <p>© 2024 Anuj Dagade. All rights reserved.</p>
      </footer>
    </>
  );
};

export default Home;




// import React, { useState } from 'react';
// import { useNavigate } from 'react-router-dom';
// import Projects from './Projects';

// const Home = () => {
//   const [showMore, setShowMore] = useState(false);
//   const navigate = useNavigate();

//   const toggleMore = () => {
//     setShowMore(!showMore);
//   };

//   return (
//     <>
//       <div className='pt-4 mt-10 flex justify-center'>
//         <p className='text-2xl text-red-600'>Under development</p>
//       </div>

//       {/* Header */}
//       <header className="relative z-10 text-center mt-20">
//         <h1 className="text-6xl font-extrabold text-blue-400 mb-4 glow-md tracking-wide">Anuj Dagade</h1>
//         <p className="text-lg text-gray-300 mt-4 leading-relaxed max-w-3xl mx-auto">
//           <span className="font-bold text-blue-400">A passionate Developer</span> with expertise in building 
//           <span className="text-blue-400 font-semibold"> scalable applications</span> and 
//           <span className="text-blue-400 font-semibold"> modern user interfaces</span>. 
//           With a deep interest in <span className="font-semibold text-pink-400">problem-solving</span> and 
//           <span className="font-semibold text-pink-400"> backend architecture</span>, 
//           I love working on innovative projects that make an impact. When I'm not coding, I'm exploring new tools 
//           {/*contributing to open-source,*/} or experimenting with <span className="font-semibold text-red-400">automation in gaming</span>!
//         </p>
//       </header>

//       {/* Stylish Skills Section */}
//       <section className="relative z-10 mt-10 text-center">
//         <h2 className="text-4xl font-bold text-blue-400 mb-4">Skills</h2>
//         <div className="grid grid-cols-1 md:grid-cols-2 gap-4 text-black">
//           <div>
//             <h3 className="text-xl font-semibold text-blue-500 mb-2">Frontend</h3>
//             <p className="bg-blue-600 rounded-full py-1 px-3 inline-block">React</p>
//             <p className="bg-blue-600 rounded-full py-1 px-3 inline-block ml-2">Angular</p>
//             <p className="bg-blue-600 rounded-full py-1 px-3 inline-block ml-2">JavaScript</p>
//             <p className="bg-blue-600 rounded-full py-1 px-3 inline-block ml-2">HTML</p>
//             <p className="bg-blue-600 rounded-full py-1 px-3 inline-block ml-2">CSS</p>
//           </div>
//           <div>
//             <h3 className="text-xl font-semibold text-green-500 mb-2">Backend</h3>
//             <p className="bg-green-600 rounded-full py-1 px-3 inline-block">Node.js</p>
//             <p className="bg-green-600 rounded-full py-1 px-3 inline-block ml-2">Java</p>
//             <p className="bg-green-600 rounded-full py-1 px-3 inline-block ml-2">Spring</p>
//             <p className="bg-green-600 rounded-full py-1 px-3 inline-block ml-2">Express.js</p>
//           </div>
//           <div>
//             <h3 className="text-xl font-semibold text-purple-500 mb-2">Databases</h3>
//             <p className="bg-purple-600 rounded-full py-1 px-3 inline-block">MongoDB</p>
//             <p className="bg-purple-600 rounded-full py-1 px-3 inline-block ml-2">Postgresql</p>
//             {/* <p className="bg-purple-600 rounded-full py-1 px-3 inline-block ml-2">Neo4j</p> */}
//           </div>
//           <div>
//             <h3 className="text-xl font-semibold text-yellow-500 mb-2">Others</h3>
//             <p className="bg-yellow-600 rounded-full py-1 px-3 inline-block">Python</p>
//             <p className=" ml-2 bg-yellow-600 rounded-full py-1 px-3 inline-block">Bash</p>
//             <p className="ml-2 bg-yellow-600 rounded-full py-1 px-3 inline-block">Git</p>
//             <p className="bg-yellow-600 rounded-full py-1 px-3 inline-block ml-2">Docker</p>
//             <p className="bg-yellow-600 rounded-full py-1 px-3 inline-block ml-2">Linux</p>
//             <p className="bg-yellow-600 rounded-full py-1 px-3 inline-block ml-2">REST APIs</p>
//           </div>
//         </div>
//       </section>

//       {/* Projects Section */}
//       <section className="relative z-10 mt-10">
//         <Projects showMore={showMore} numProjects={2}/>
//         <button 
//           onClick={()=> navigate("/projects")} 
//           className="mt-4 bg-blue-500 hover:bg-blue-600 text-white font-semibold py-2 px-4 rounded transition duration-300 ml-5"
//         >
//           {showMore ? "Show Less" : "Show More"}
//         </button>
//       </section>

//       {/* Footer */}
//       <footer className="relative z-10 mt-20 text-center text-gray-500">
//         <p>© 2024 Anuj Dagade. All rights reserved.</p>
//         <div className="mt-4 flex justify-center space-x-6">
//           <a href="#" className="text-blue-400 hover:text-blue-600 transition duration-300">LinkedIn</a>
//           <a href="#" className="text-blue-400 hover:text-blue-600 transition duration-300">GitHub</a>
//         </div>
//       </footer>
//     </>
//   );
// };

// export default Home;
