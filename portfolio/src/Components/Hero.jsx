import React from 'react'
import profileImg from "../assets/profile.jpg";
import Button from './Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGithub } from '@fortawesome/free-brands-svg-icons/faGithub';



export default function Hero() {
    return (
        <>
            <div className="flex w-full lg:flex-row flex-col items-center lg:items-start bg-gradient-to-r from-purple-600 via-pink-500 to-red-500 p-10 lg:p-20 rounded-lg shadow-lg">
                <div className="lg:h-3/4 lg:w-1/2 lg:ml-10 text-gray-100 pt-14 ">
                    <h3 className="lg:text-4xl text-2xl font-bold mb-4">HI, I'm your friendly neighborhood Full-Stack Developer
                        <span className="text-blue-300"> Anuj Dagade.</span></h3>
                    <p className="mt-4 lg:text-lg text-md">
                        Welcome to my portfolio. I am passionate about creating beautiful, functional, and robust websites, both front-end and back-end.
                        I am always looking to learn new technologies and improve my skills.
                    </p>
                    <Button look={'mt-10 bg-blue-500 text-white px-6 py-3 rounded-lg shadow-md hover:bg-blue-400 transition-transform transform hover:scale-105'}>
                        <a target='_blank' rel="noopener noreferrer" href="https://www.linkedin.com/in/anuj-dagade/">Get in touch</a>
                    </Button>
                    <a href="https://github.com/your-username" target="_blank" rel="noopener noreferrer">
                        <FontAwesomeIcon icon={faGithub} shake size='2x' className='mt-12 ml-5' />
                    </a>

                </div>
                <img className="lg:h-96 lg:w-96 lg:mt-10 lg:ml-10 rounded-lg shadow-lg transform hover:scale-105 transition-transform" src={profileImg} alt="Photo of me" />
            </div>
        </>
    )
}

