import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import useScrollToNavigate from '../hooks/useScrollToNavigate';
const NavBar = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };
  // useScrollToNavigate(["/home","/projects","/contact","/about"]);


  return (
    <nav className=" bg-opacity-80 backdrop-blur-md w-full py-4 px-6 fixed top-0 z-20 shadow-md transition duration-300">
      <div className="container mx-auto flex justify-between items-center">
        {/* Logo */}
        <div className="text-blue-400 font-bold text-2xl">
          Anuj Dagade
        </div>

        {/* Hamburger Menu for Mobile */}
        <div className="md:hidden">
          <button onClick={toggleMenu} className="text-gray-300 focus:outline-none">
            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 6h16M4 12h16m-7 6h7" />
            </svg>
          </button>
        </div>

        {/* Nav Links */}
        <div className={`flex-col md:flex-row md:flex md:space-x-8 ${isOpen ? 'flex' : 'hidden'} md:flex`}>
          <Link 
            to="/home" 
            className="text-gray-300 px-3 py-2 rounded-md transition-colors duration-300 transform hover:scale-105 hover:bg-blue-400 hover:text-white hover:underline decoration-pink-400"
          >
            Home
          </Link>
          <Link 
            to="/projects" 
            className="text-gray-300 px-3 py-2 rounded-md transition-colors duration-300 transform hover:scale-105 hover:bg-blue-400 hover:text-white hover:underline decoration-yellow-400"
          >
            Projects
          </Link>
          <Link 
            to="/contact" 
            className="text-gray-300 px-3 py-2 rounded-md transition-colors duration-300 transform hover:scale-105 hover:bg-blue-400 hover:text-white hover:underline decoration-green-400"
          >
            Contact
          </Link>
          <Link 
            to="/about" 
            className="text-gray-300 px-3 py-2 rounded-md transition-colors duration-300 transform hover:scale-105 hover:bg-blue-400 hover:text-white hover:underline decoration-purple-400"
          >
            About
          </Link>
        </div>
      </div>
    </nav>
  );
};

export default NavBar;



// import React, { useState } from 'react';
// import { Link } from 'react-router-dom';

// const NavBar = () => {
//   const [isOpen, setIsOpen] = useState(false);

//   const toggleMenu = () => {
//     setIsOpen(!isOpen);
//   };

//   return (
//     <nav className="bg-transparent bg-opacity-75 backdrop-blur-md w-full py-4 px-6 fixed top-0 z-20 shadow-md transition duration-300 ">
//       <div className="container mx-auto flex justify-between items-center">
//         {/* Logo */}
//         <div className="text-blue-400 font-bold text-2xl">
//           Anuj Dagade
//         </div>

//         {/* Hamburger Menu for Mobile */}
//         <div className="md:hidden">
//           <button onClick={toggleMenu} className="text-gray-300 focus:outline-none">
//             <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
//               <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 6h16M4 12h16m-7 6h7" />
//             </svg>
//           </button>
//         </div>

//         {/* Nav Links */}
//         <div className={`flex-col md:flex-row md:flex md:space-x-8 text-gray-300 ${isOpen ? 'flex' : 'hidden'} md:flex`}>
//           <Link to="/home" className="hover:text-blue-400 transition-colors duration-300">Home</Link>
//           <Link to="/projects" className="hover:text-blue-400 transition-colors duration-300">Projects</Link>
//           <Link to="/contact" className="hover:text-blue-400 transition-colors duration-300">Contact</Link>
//           <Link to="#about" className="hover:text-blue-400 transition-colors duration-300">About</Link>
//         </div>
//       </div>
//     </nav>
//   );
// };

// export default NavBar;
