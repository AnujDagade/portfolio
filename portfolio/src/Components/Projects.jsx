import React from 'react';

const Projects = ({ numProjects }) => {
  // Array of project data
  const projectData = [
    {
      title: "EduAsses",
      description: "A learning management system enabling quizzes, notes uploads, and more.",
    },
    {
      title: "GetHope",
      description: "A platform connecting domestic abuse victims with NGOs.",
    },
    {
      title: "Project X",
      description: "A project focused on automation and innovation.",
    },
    {
      title: "Project Y",
      description: "An AI-based platform for interactive learning.",
    },
    // Add more projects as needed
  ];

  // Slice the projectData array to limit the number of displayed projects based on the prop
  const displayedProjects = projectData.slice(0, numProjects);

  return (
    <section id="projects" className="relative z-10 mt-16 w-full max-w-4xl px-6">
      <h2 className="text-4xl font-semibold mb-8 text-center text-gray-200">Projects</h2>

      <div className="grid grid-cols-1 md:grid-cols-2 gap-8">
        {/* Map over the displayed projects */}
        {displayedProjects.map((project, index) => (
          <div 
            key={index} 
            className="bg-gray-800 rounded-xl p-6 hover:scale-105 hover:shadow-2xl transition-all duration-300"
          >
            <h3 className="text-3xl font-bold text-blue-400 mb-2">{project.title}</h3>
            <p className="text-gray-300">{project.description}</p>
          </div>
        ))}
      </div>
    </section>
  );
};

export default Projects;
