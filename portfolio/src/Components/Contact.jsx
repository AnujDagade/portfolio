import React, { useState } from 'react';

const Contact = () => {
  const [formData, setFormData] = useState({ name: '', email: '', message: '' });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(formData);
  };

  return (
    <div className=" text-white min-h-screen flex flex-col items-center justify-center p-6 relative">
      {/* Header */}
      <h1 className="text-5xl font-bold text-blue-400 mb-6 z-20">Contact Me</h1>
      <p className="text-lg text-gray-300 mb-8 text-center max-w-2xl z-20">
        I'm here to help and answer any questions you might have. Reach out to me directly or use the form below!
      </p>

      {/* Contact Information */}
      <div className="mb-12 text-center z-20">
        <p className="text-lg font-semibold text-gray-300 mb-4">My preferred contact methods</p>
        
        <div className="flex flex-col items-center space-y-2">
          <p className="text-gray-400">
            Email: <a href="dagadeanuj@gmail.com" className="text-blue-400 hover:underline">dagadeanuj@gmail.com</a>
          </p>
          {/* <p className="text-gray-400">
            Phone: <a href="tel:+1234567890" className="text-blue-400 hover:underline">+123 456 7890</a>
          </p> */}
          <a 
          href="https://linkedin.com/in/anuj-dagade" 
          target="_blank" 
          rel="noopener noreferrer" 
          className="flex items-center space-x-2 text-blue-400 hover:text-blue-500 transition duration-300"
          >       
          <svg 
          xmlns="http://www.w3.org/2000/svg" 
          width="24" 
          height="24" 
          fill="currentColor" 
          viewBox="0 0 24 24"
          className="inline-block"
          >
    <path d="M19 0h-14c-2.76 0-5 2.24-5 5v14c0 2.76 2.24 5 5 5h14c2.76 0 5-2.24 5-5v-14c0-2.76-2.24-5-5-5zm-11 19h-3v-10h3v10zm-1.5-11.28c-.97 0-1.75-.78-1.75-1.72s.78-1.72 1.75-1.72 1.75.78 1.75 1.72-.78 1.72-1.75 1.72zm13.5 11.28h-3v-5.6c0-1.35-.03-3.07-1.87-3.07-1.87 0-2.15 1.46-2.15 2.97v5.7h-3v-10h2.88v1.37h.04c.4-.76 1.38-1.55 2.84-1.55 3.04 0 3.6 2 3.6 4.53v5.65z" />
  </svg>
  <span>LinkedIn</span>
</a>

          <a 
            href="https://github.com/AnujDagade" 
            target="_blank" 
            rel="noopener noreferrer" 
            className="flex items-center space-x-2 text-blue-400 hover:text-blue-500 transition duration-300"
          >
            <svg 
              xmlns="http://www.w3.org/2000/svg" 
              width="24" 
              height="24" 
              fill="currentColor" 
              viewBox="0 0 24 24"
              className="inline-block"
            >
              <path d="M12 .5C5.37.5 0 5.87 0 12.48c0 5.29 3.437 9.77 8.207 11.36.6.114.793-.26.793-.577v-2.24c-3.34.725-4.037-1.61-4.037-1.61-.546-1.385-1.334-1.755-1.334-1.755-1.09-.746.082-.73.082-.73 1.204.087 1.836 1.263 1.836 1.263 1.07 1.843 2.807 1.31 3.492.996.108-.775.42-1.31.764-1.61-2.667-.306-5.466-1.345-5.466-5.98 0-1.32.473-2.4 1.25-3.24-.123-.305-.543-1.54.116-3.207 0 0 1.008-.32 3.3 1.24a11.524 11.524 0 013.008-.404c1.02 0 2.047.135 3.008.404 2.292-1.56 3.3-1.24 3.3-1.24.66 1.667.24 2.902.12 3.207.78.84 1.25 1.92 1.25 3.24 0 4.647-2.8 5.67-5.47 5.97.43.36.81 1.1.81 2.227v3.3c0 .317.2.7.8.58C20.56 22.24 24 17.77 24 12.48 24 5.87 18.63.5 12 .5z" />
            </svg>
            <span>GitHub</span>
          </a>
        </div>
      </div>

      {/* Contact Form */}
      <form onSubmit={handleSubmit} className="bg-gray-800 p-8 rounded-lg shadow-lg max-w-md w-full space-y-6 z-20">
        <div>
          <label className="block text-gray-300 mb-2 font-semibold" htmlFor="name">Name</label>
          <input 
            type="text" 
            id="name" 
            name="name" 
            value={formData.name} 
            onChange={handleChange} 
            className="w-full p-3 rounded bg-gray-700 border border-gray-600 text-white focus:outline-none focus:border-blue-500" 
            required 
          />
        </div>

        <div>
          <label className="block text-gray-300 mb-2 font-semibold" htmlFor="email">Email</label>
          <input 
            type="email" 
            id="email" 
            name="email" 
            value={formData.email} 
            onChange={handleChange} 
            className="w-full p-3 rounded bg-gray-700 border border-gray-600 text-white focus:outline-none focus:border-blue-500" 
            required 
          />
        </div>

        <div>
          <label className="block text-gray-300 mb-2 font-semibold" htmlFor="message">Message</label>
          <textarea 
            id="message" 
            name="message" 
            value={formData.message} 
            onChange={handleChange} 
            rows="4" 
            className="w-full p-3 rounded bg-gray-700 border border-gray-600 text-white focus:outline-none focus:border-blue-500" 
            required 
          />
        </div>

        <button 
          type="submit" 
          className="w-full bg-blue-500 hover:bg-blue-600 transition duration-300 p-3 rounded font-semibold text-white"
        >
          Send Message
        </button>
      </form>
    </div>
  );
};

export default Contact;



// import React, { useState } from 'react';

// const Contact = () => {
//   const [formData, setFormData] = useState({ name: '', email: '', message: '' });

//   const handleChange = (e) => {
//     const { name, value } = e.target;
//     setFormData({ ...formData, [name]: value });
//   };

//   const handleSubmit = (e) => {
//     e.preventDefault();
//     console.log(formData);
//   };

//   return (
//     <div className="bg-gray-900 text-white min-h-screen flex flex-col items-center justify-center p-6">
//       {/* Header */}
//       <h1 className="text-5xl font-bold text-blue-400 mb-6">Contact Me</h1>
//       <p className="text-lg text-gray-300 mb-12 text-center max-w-2xl">
//         I'm here to help and answer any questions you might have. Reach out to me using the form below or through my contact details!
//       </p>

//       {/* Contact Form */}
//       <form onSubmit={handleSubmit} className="bg-gray-800 p-8 rounded-lg shadow-lg max-w-md w-full space-y-6">
//         <div>
//           <label className="block text-gray-300 mb-2 font-semibold" htmlFor="name">Name</label>
//           <input 
//             type="text" 
//             id="name" 
//             name="name" 
//             value={formData.name} 
//             onChange={handleChange} 
//             className="w-full p-3 rounded bg-gray-700 border border-gray-600 text-white focus:outline-none focus:border-blue-500" 
//             required 
//           />
//         </div>

//         <div>
//           <label className="block text-gray-300 mb-2 font-semibold" htmlFor="email">Email</label>
//           <input 
//             type="email" 
//             id="email" 
//             name="email" 
//             value={formData.email} 
//             onChange={handleChange} 
//             className="w-full p-3 rounded bg-gray-700 border border-gray-600 text-white focus:outline-none focus:border-blue-500" 
//             required 
//           />
//         </div>

//         <div>
//           <label className="block text-gray-300 mb-2 font-semibold" htmlFor="message">Message</label>
//           <textarea 
//             id="message" 
//             name="message" 
//             value={formData.message} 
//             onChange={handleChange} 
//             rows="4" 
//             className="w-full p-3 rounded bg-gray-700 border border-gray-600 text-white focus:outline-none focus:border-blue-500" 
//             required 
//           />
//         </div>

//         <button 
//           type="submit" 
//           className="w-full bg-blue-500 hover:bg-blue-600 transition duration-300 p-3 rounded font-semibold text-white"
//         >
//           Send Message
//         </button>
//       </form>

//       {/* Contact Information */}
//       <div className="mt-12 text-center">
//         <p className="text-lg font-semibold text-gray-300 mb-4">Or contact me directly:</p>
        
//         {/* Contact Details with GitHub Link */}
//         <div className="flex flex-col items-center space-y-2">
//           <p className="text-gray-400">
//             Email: <a href="mailto:example@domain.com" className="text-blue-400 hover:underline">example@domain.com</a>
//           </p>
//           <p className="text-gray-400">
//             Phone: <a href="tel:+1234567890" className="text-blue-400 hover:underline">+123 456 7890</a>
//           </p>

//           {/* GitHub Link with Logo */}
//           <a 
//             href="https://github.com/username" 
//             target="_blank" 
//             rel="noopener noreferrer" 
//             className="flex items-center space-x-2 text-blue-400 hover:text-blue-500 transition duration-300"
//           >
//             <svg 
//               xmlns="http://www.w3.org/2000/svg" 
//               width="24" 
//               height="24" 
//               fill="currentColor" 
//               viewBox="0 0 24 24"
//               className="inline-block"
//             >
//               <path d="M12 .5C5.37.5 0 5.87 0 12.48c0 5.29 3.437 9.77 8.207 11.36.6.114.793-.26.793-.577v-2.24c-3.34.725-4.037-1.61-4.037-1.61-.546-1.385-1.334-1.755-1.334-1.755-1.09-.746.082-.73.082-.73 1.204.087 1.836 1.263 1.836 1.263 1.07 1.843 2.807 1.31 3.492.996.108-.775.42-1.31.764-1.61-2.667-.306-5.466-1.345-5.466-5.98 0-1.32.473-2.4 1.25-3.24-.123-.305-.543-1.54.116-3.207 0 0 1.008-.32 3.3 1.24a11.524 11.524 0 013.008-.404c1.02 0 2.047.135 3.008.404 2.292-1.56 3.3-1.24 3.3-1.24.66 1.667.24 2.902.12 3.207.78.84 1.25 1.92 1.25 3.24 0 4.647-2.8 5.67-5.47 5.97.43.36.81 1.1.81 2.227v3.3c0 .317.2.7.8.58C20.56 22.24 24 17.77 24 12.48 24 5.87 18.63.5 12 .5z" />
//             </svg>
//             <span>GitHub</span>
//           </a>
//         </div>
//       </div>
//     </div>
//   );
// };

// export default Contact;
