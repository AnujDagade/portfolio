import React from 'react';

const GlobalStyles = ({ children }) => {
  return (
    <div className="bg-gray-900 text-white min-h-screen flex flex-col items-center justify-center relative">
      {/* Background geometric shapes */}
      <div className="absolute top-0 left-0 w-full h-full overflow-hidden">
        <div className="bg-blue-600 opacity-30 w-64 h-64 rounded-full absolute top-20 left-20 blur-3xl"></div>
        <div className="bg-pink-500 opacity-40 w-48 h-48 rounded-full absolute bottom-20 right-20 blur-3xl"></div>
      </div>

      {children}
    </div>
  );
};

export default GlobalStyles;
