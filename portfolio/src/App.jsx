import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import NavBar from './Components/NavBar'; // Make sure the path is correct
import Home from './Components/Home'; // Import Home component
import Projects from './Components/Projects'; // Import Projects component
import About from './Components/About'
import GlobalStyles from './Components/GlobalStyles';
import Contact from './Components/Contact';
import useScrollToNavigate from './hooks/useScrollToNavigate';
function App() {
  return (
    <Router>
      <NavBar />
      <GlobalStyles>
      <div className="mt-16"> {/* Add margin to prevent overlap with the navbar */}
        <Routes>
          <Route path="/home" element={<Home />} />
          <Route path="/projects" element={<Projects numProjects={2} />} />
          <Route path="/contact" element={<Contact />} /> Add your Contact component
          <Route path="/about" element={<About />} /> {/* Add your About component */}
          <Route path="/" element={<Home />} /> {/* Default route */}
        </Routes>
      </div>
      </GlobalStyles>
    </Router>
  );
}

export default App;
